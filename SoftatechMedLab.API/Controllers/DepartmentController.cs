﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SoftatechMedLab.Services.DTOs;
using SoftatechMedLab.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SoftatechMedLab.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentsService _departmentsService;
        private readonly IMapper _mapper;

        public DepartmentController(
          IDepartmentsService departmentsService,
          IMapper mapper)
        {
            _departmentsService = departmentsService;
            _mapper = mapper;
        }


        // GET: api/<AgentController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DepartmentsDTO>>> Get()
        {
            try
            {
                return Ok(await _departmentsService.ListAsync());
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        // GET api/<AgentController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DepartmentsDTO>> Get(int id)
        {
            try
            {
                return Ok(await _departmentsService.GetByIdAsync(id));
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        // POST api/<AgentController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] DepartmentsDTO department)
        {
            try
            {
                await _departmentsService.AddAsync(department);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        // PUT api/<AgentController>
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] DepartmentsDTO department)
        {
            try
            {
                await _departmentsService.UpdateAsync(department);
                return Created("", "");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        // DELETE api/<AgentController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _departmentsService.DeleteAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
