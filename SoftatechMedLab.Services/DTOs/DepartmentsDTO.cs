﻿using System;

namespace SoftatechMedLab.Services.DTOs
{
    public class DepartmentsDTO
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }

        public string Description { get; set; }

        public string DeptCode { get; set; }
    }
}
