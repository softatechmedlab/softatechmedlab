﻿using AutoMapper;
using SoftatechMedLab.Infrastructure.Entities;
using SoftatechMedLab.Infrastructure.Interfaces;
using SoftatechMedLab.Services.DTOs;
using SoftatechMedLab.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SoftatechMedLab.Services.Implementations
{
    public class DepartmentsService : IDepartmentsService
    {
        private readonly IMapper _mapper;
        private readonly ISoftatechMedLabRepository _softatechMedLabRepository;

        public DepartmentsService(ISoftatechMedLabRepository softatechMedLabRepository, IMapper mapper)
        {
            _mapper = mapper;
            _softatechMedLabRepository = softatechMedLabRepository;
        }

        public async Task AddAsync(DepartmentsDTO dto)
        {
            await _softatechMedLabRepository.AddAsync<Departments>(_mapper.Map<Departments>(dto));
        }

        public async Task DeleteAsync(int id)
        {
            await _softatechMedLabRepository.DeleteAsync<Departments>(id);
        }

        public async Task<DepartmentsDTO> GetByIdAsync(int id)
        {
           var department = await _softatechMedLabRepository.GetByIdAsync<Departments>(id);
            return _mapper.Map<DepartmentsDTO>(department);
        }

        public async Task<List<DepartmentsDTO>> ListAsync()
        {
            var departmentList = await _softatechMedLabRepository.ListAsync<Departments>();
            return _mapper.Map<List<DepartmentsDTO>>(departmentList);
        }

        public async Task UpdateAsync(DepartmentsDTO dto)
        {
            await _softatechMedLabRepository.UpdateAsync<Departments>(_mapper.Map<Departments>(dto));
        }
    }
}
