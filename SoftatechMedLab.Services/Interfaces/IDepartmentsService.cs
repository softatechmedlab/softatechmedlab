﻿using SoftatechMedLab.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SoftatechMedLab.Services.Interfaces
{
    public interface IDepartmentsService
    {
        Task<DepartmentsDTO> GetByIdAsync(int id);
        Task<List<DepartmentsDTO>> ListAsync();
        Task AddAsync(DepartmentsDTO dto);
        Task UpdateAsync(DepartmentsDTO dto);
        Task DeleteAsync(int id);
    }
}
