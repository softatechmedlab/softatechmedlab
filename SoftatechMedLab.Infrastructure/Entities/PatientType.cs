﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class PatientType : BaseEntity
    {        
        public string PatientTypeName { get; set; }

    }
}
