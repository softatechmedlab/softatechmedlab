﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class TestItemDefinition: BaseEntity 
    {        

        public long? TestCasesDefinitionId { get; set; }

        public string TestItem { get; set; }

        public string Tag { get; set; }

        public short? UnitId { get; set; }

        public bool IsBoolean { get; set; }

        public short? TestOutcomeTypeId { get; set; }

    }

}
