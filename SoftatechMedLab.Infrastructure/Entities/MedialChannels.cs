﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class MedialChannels: BaseEntity
    {        
        public string PatientNumber { get; set; }

        public byte? MedialChannelTypeId { get; set; }

        public string Handle { get; set; }

    }
}
