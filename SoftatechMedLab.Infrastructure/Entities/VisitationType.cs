﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class VisitationType: BaseEntity
    {
        public string VisitationTypeName { get; set; }
    }
}
