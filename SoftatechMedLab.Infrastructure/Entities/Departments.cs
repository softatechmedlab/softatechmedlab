﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class Departments: BaseEntity
    {
        public string DepartmentName { get; set; }

        public string Description { get; set; }

        public string DeptCode { get; set; }
    }
}
