﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class Request: BaseEntity
    {
        public string PatientNumber { get; set; }

        public int PatientVisitId { get; set; }

        public string Complaints { get; set; }
    }
}
