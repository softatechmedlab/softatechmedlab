﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class Units: BaseEntity
    {
        public string DepartmentId { get; set; }

        public string SubUnitName { get; set; }

        public string Description { get; set; }

        public string SubUnitCode { get; set; }
    }
}
