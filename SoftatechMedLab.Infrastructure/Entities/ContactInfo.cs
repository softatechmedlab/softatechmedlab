﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class ContactInfo: BaseEntity
    {
        public string PatientNumber { get; set; }

        public string RegularPhone { get; set; }

        public string OfficePhone { get; set; }

        public string HomePhone { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string EmailAddress { get; set; }

    }

}
