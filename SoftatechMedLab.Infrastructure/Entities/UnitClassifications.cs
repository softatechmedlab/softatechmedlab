﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class UnitClassifications: BaseEntity
    {
        public string UnitClass { get; set; }
    }
}