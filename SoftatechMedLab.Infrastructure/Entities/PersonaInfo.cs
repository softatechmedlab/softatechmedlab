﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class PersonaInfo : BaseEntity
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public byte GenderId { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public int? Age { get; set; }

        public string AgeGroup { get; set; }

        public byte? MarritalStatusId { get; set; }

        public byte? NumberOfChildren { get; set; }

        public string Height { get; set; }

        public string BloodGroup { get; set; }

        public string GenoType { get; set; }

        public bool? Disability { get; set; }

        public byte? DisabilityId { get; set; }

        public string PatientNumber { get; set; }

    }
}
