﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class Patient_TestCases: BaseEntity
    {
        public string PatientNumber { get; set; }

        public long? TestCasesDefinitionId { get; set; }

        public int? TestItemDefinitionId { get; set; }

        public int PatientVisitId { get; set; }

        public DateTime? DateTimeCreated { get; set; }

        public byte TestStatusId { get; set; }
    }
}
