﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class TestCasesDefinition: BaseEntity
    {
        public string TestCaseTitle { get; set; }

        public bool TemplateVisibility { get; set; }
    }
}
