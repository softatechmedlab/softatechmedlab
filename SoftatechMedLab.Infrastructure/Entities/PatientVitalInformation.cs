﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class PatientVitalInformation : BaseEntity
    {
        public string PatientNumber { get; set; }

        public int PatientVisitId { get; set; }

        public int? VitalInfomationId { get; set; }

        public string OutcomeValue { get; set; }

        public string LowValue { get; set; }

        public string HighValue { get; set; }
    }
}
