﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class MedialChannelType: BaseEntity 
    {        

        public string MediaChannel { get; set; }

    }

}
