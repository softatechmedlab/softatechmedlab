﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class Patient_TestResult: BaseEntity
    {
        public string PatientNumber { get; set; }

        public long? TestCasesDefinitionId { get; set; }

        public int? TestItemDefinitionId { get; set; }

        public string Result { get; set; }

        public string LowValue { get; set; }

        public string HighValue { get; set; }
    }
}
