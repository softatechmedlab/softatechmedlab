﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class Status: BaseEntity
    {
        public string Statuscategory { get; set; }

        public string StatusName { get; set; }
    }
}
