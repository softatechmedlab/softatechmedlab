﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class PatientVisits : BaseEntity
    {        

        public string PatientNumber { get; set; }

        public byte VisitationTypeId { get; set; }

        public DateTime? LastAppointmentDate { get; set; }

        public DateTime ScheduledVisitDate { get; set; }

        public DateTime? NextVisitDate { get; set; }

        public byte PatientTypeId { get; set; }

    }

}
