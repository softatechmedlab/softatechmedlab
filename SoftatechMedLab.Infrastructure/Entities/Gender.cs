﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class Gender: BaseEntity
    {
        public string GenderName { get; set; }

        public string GenderCode { get; set; }
    }
}
