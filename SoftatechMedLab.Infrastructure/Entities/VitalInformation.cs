﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class VitalInformation : BaseEntity
    {
        public string Item { get; set; }

        public short? OutcomeTypeId { get; set; }
    }
}