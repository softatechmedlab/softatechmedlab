﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class TestOutcomeRange: BaseEntity
    {
        public int? TestItemDefinitionId { get; set; }

        public string Low { get; set; }

        public string High { get; set; }

        public string NormalLow { get; set; }

        public string NormalHigh { get; set; }

        public string RangeSeperator { get; set; }

        public string NormalValue { get; set; }

    }
}
