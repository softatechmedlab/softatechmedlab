﻿using System;

namespace SoftatechMedLab.Infrastructure.Entities
{
    public class TestOutcomeType: BaseEntity
    {
        public string OutcomeType { get; set; }

        public string Measurement { get; set; }

        public string OutcomeDataType { get; set; }

        public string OutcomeSymbol2 { get; set; }
    }
}
