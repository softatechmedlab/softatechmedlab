﻿using SoftatechMedLab.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SoftatechMedLab.Infrastructure.Interfaces
{
    public interface ISoftatechMedLabRepository
    {
        Task<T> GetByIdAsync<T>(int id) where T : BaseEntity;       
        Task<List<T>> ListAsync<T>() where T : BaseEntity;        
        Task<T> AddAsync<T>(T entity) where T : BaseEntity;
        Task UpdateAsync<T>(T entity) where T : BaseEntity;
        Task DeleteAsync<T>(T entity) where T : BaseEntity;
        Task DeleteAsync<T>(int id) where T : BaseEntity;
    }
}
