﻿using Microsoft.EntityFrameworkCore;
using SoftatechMedLab.Infrastructure.Entities;

namespace SoftatechMedLab.Infrastructure.Data
{
    public class SoftatechMedLabContext : DbContext
    {
        public SoftatechMedLabContext(DbContextOptions<SoftatechMedLabContext> options): base(options)
        {
            
        }
        
        public DbSet<ContactInfo> ContactInfo { get; set; }
        public DbSet<Departments> Departments { get; set; }
        public DbSet<Gender> Gender { get; set; }
        public DbSet<MaritalStatus> MaritalStatus { get; set; }
        public DbSet<MedialChannels> MedialChannels { get; set; }
        public DbSet<MedialChannelType> MedialChannelType { get; set; }
        public DbSet<Patient_TestCases> Patient_TestCases { get; set; }
        public DbSet<Patient_TestResult> Patient_TestResult { get; set; }
        public DbSet<PatientType> PatientType { get; set; }
        public DbSet<PatientVisits> PatientVisits { get; set; }
        public DbSet<PatientVitalInformation> PatientVitalInformation { get; set; }
        public DbSet<PersonaInfo> PersonaInfo { get; set; }
        public DbSet<Request> Request { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<TestCasesDefinition> TestCasesDefinition { get; set; }
        public DbSet<TestOutcomeRange> TestOutcomeRange { get; set; }
        public DbSet<TestOutcomeType> TestOutcomeType { get; set; }
        public DbSet<UnitClassifications> UnitClassifications { get; set; }
        public DbSet<Units> Units { get; set; }
        public DbSet<VisitationType> VisitationType { get; set; }
        public DbSet<VitalInformation> VitalInformation { get; set; }
    }
}
